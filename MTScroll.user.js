// ==UserScript==
// @name     MTScroll
// @namespace MTScroll
// @match        https://www.meistertask.com/app/project/*
// @require   https://cdnjs.cloudflare.com/ajax/libs/velocity/1.5.0/velocity.js
// @run-at   document-idle
// ==/UserScript==
// taken from: MTScroll v0.899 

/* CHANGE AS YOU LIKE */
var initTime = 7500;        	// (milliseconds)	time for the preloader to do it's thing
var prepareWaitTime = 5000;		// (ms)	time for the preloader to finally do it's thing (this had to be somehow working as of 2018-04-23)
var offsetDistance = 0;        	// (px), probably does not work.
var delayTime = 6000;          	// (ms)
var requestedCycles = 8;      	// 1 cycle = 1*(scroll right, then left)
var scrollFactor = 1000;    	// the more, the faster.
var emergencyTimer = 300000;	// (ms) strg+f "Stability Mode"
/* */

/* DO NOT CHANGE */
var diagnostics;
var cycle;
var timeoutSet = 0;
/* */

/* DEBUG CONFIGURATION: */
// var delayTime = 1000; var requestedCycles = 10; var scrollFactor = 100; var diagnostics = 1; console.log("DIAGNOSTICS MODE ACTIVATED");

/* 'Stability Mode' (sure-fire Timeout for the page reload - comment if unnecessary.) */
setTimeout(function(){window.location.reload(true);},emergencyTimer);

/* waiting for the preloading process to finish */
setTimeout(function(){
	Prepare();
}, initTime);
/**/


/* beautify the project page, then call the function that calls velocity.js for scrolling the page back and forth */
function Prepare(){
    if(diagnostics == 1){console.log("Prepare() ...");}

    setTimeout(function(){
	    document.querySelector("div#DOM_CONTAINER > div.kr-view > div.kr-view:nth-child(2) > div.kr-view > div.kr-view > div.kr-view:nth-child(1)").style.display = "none";		// HIDE: white top horizontal bar
	    document.querySelector("div#DOM_CONTAINER > div.kr-view > div:nth-child(2) > div.kr-view > div.kr-view > div.kr-view:nth-child(2) > div.kr-view:nth-child(2)").style.display = "none";	// HIDE: black vertical bar to the right
	    document.querySelector("div#DOM_CONTAINER > div.kr-view > div:nth-child(2) > div.kr-view > div.kr-view > div.kr-view:nth-child(2) > div.kr-view:nth-child(1) > div > div:first-child").style.right = "0px";	// HIDE: ugly blue-ish (default settings) bar to the right left left over from .l-side-panel
	    document.querySelector("div#DOM_CONTAINER > div.kr-view > div:nth-child(2) > div.kr-view > div.kr-view > div.kr-view:nth-child(2) > div.kr-view:nth-child(1)").style.right = "0px";
//		document.querySelector("div.v-section-column.js-fake-section.ui-fake-section").childNodes.forEach(function(element){element.style.display = "none";}); // HIDE: just to be sure (???)
//		document.querySelectorAll("div.js-empty-placeholder.ui-empty-placeholder").forEach(function(element){element.style.display = "none";}); // HIDE: "no tasks left"-iconography
//		document.querySelectorAll("span.ui-adder.h-cp").forEach(function(element){element.style.display = "none";}); // HIDE: 'add task'-buttons
	    if(diagnostics == 1){ console.log('Prepare()ations done.' );}
    }, prepareWaitTime);

    RoundTrip(); 		// start scroll loop
}
/**/

function RoundTrip(){
	if(diagnostics == 1){ console.log('RoundTrip() was called');}

	// gathering information...
    var context = document.querySelector('div#DOM_CONTAINER > div.kr-view > div:nth-child(2) > div.kr-view > div.kr-view > div.kr-view:nth-child(2) > div.kr-view:nth-child(1) > div > div:first-child');		// what to scroll
    var targets = document.querySelectorAll('div#DOM_CONTAINER > div.kr-view > div:nth-child(2) > div.kr-view > div.kr-view > div.kr-view:nth-child(2) > div.kr-view:nth-child(1) > div > div > div:first-child > div');      // columns
    var scrollTime = targets.length * scrollFactor;											// scrollTime = (number of columns * scrollFactor)
    var lastCol = targets.item(targets.length - 2 );										// there are 2 columns that are not part of the Scroll.
    var totalTime = (initTime + 2*scrollTime*requestedCycles + 2*delayTime*requestedCycles);// total scrolling Time for the number of requestedCycles
	console.log('time until refresh (totalTime): ' + totalTime + " milliseconds" );

	/* DEBUG */
	if(diagnostics == 1){
		console.log('context found: ' + context);
		console.log('targets found: ' + targets.length);
		console.log("scrollTime = " + scrollTime);
		console.log('lastCol found: ' + lastCol);
	}
	/**/

	/* set timeout for reloading the page. needs rethinking, for sure, but it works now at least. */
    if(timeoutSet === 0){
        setTimeout(function(totalTime){window.location.reload(true);
        timeoutSet++;}, totalTime);
    }
    /**/

    /* DEBUG */
    if (diagnostics == 1) {
  		if( context === undefined ) {console.log('[RoundTrip() scope] context is === undefined: ' + context);} else {console.log('[RoundTrip() scope] everything === fine! ' + context);
  		if( delayTime === undefined || lastCol === undefined || targets === undefined || cycle === undefined ){
		    console.log('[RoundTrip() scope] something was === undefined:');
		    console.log('delayTime: ' + delayTime);
		    console.log('lastCol: ' + lastCol);
		    console.log('targets: ' + targets);
		} else {
			console.log('[RoundTrip() scope] everything === fine!');
		    console.log('delayTime: ' +  delayTime);
		    console.log('lastCol: ' + lastCol);
		    console.log('targets: ' + targets);
			}
		}
	}
	/**/

	/* call velocity.js */
	Velocity(lastCol, "scroll", {
		axis: 'x',
	    container: context,
	    duration: scrollTime,			// time it takes to scroll one way
	    offset: offsetDistance,			// pretty sure not effective
        delay: delayTime,				// time it waits before a scroll
		loop: requestedCycles,			// number of times it goes (back and forth)
        ease: "ease-in-out",			// not sure if effective
	    complete: function(){if(diagnostics == 1){console.log('RoundTrip(): Velocity: complete');}}
	});
}
